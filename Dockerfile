FROM node
ENV NODE_ENV=production
# Get Rust; NOTE: using sh for better compatibility with other base images
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y

# Add .cargo/bin to PATH
ENV PATH=/root/.cargo/bin:$PATH
# Check cargo is visible
RUN cargo --help
WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "npm-shrinkwrap.json*", "./"]

RUN cd /usr/src/app && npm install --production && mv node_modules ../
COPY . .
EXPOSE 5555
RUN chown -R node /usr/src/app
USER node
CMD ["npm", "start"]
