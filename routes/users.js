var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {
  res.send('respond with a resource');
});

router.put('/publishDIDDocument', async function (req, res, next) {
  let DIDDocument = req.body
  console.log("received: ");
  console.log(DIDDocument);
  const { ClientBuilder } = require('@iota/client')
  const client = new ClientBuilder().
    node('https://api.lb-0.h.chrysalis-devnet.iota.cafe')
    .build()
  client.getInfo().then(console.log).catch(console.error)
  const index = DIDDocument.id.slice(14, DIDDocument.id.length); // NOT STABIl, only in dev
  console.log(index);
  await client.message()
    .index(index)
    .data(JSON.stringify(DIDDocument))
    .submit().then(success => console.log(success)).catch(error => console.log(error));
  console.log("message:");
  res.sendStatus(200);
})

router.get('/resolveDIDDocument/:id', async function (req, res, next) {
  const index = req.params.id.slice(14, req.params.id.length); // NOT STABIl, only in dev
  console.log("index: " + index);
  const { ClientBuilder } = require('@iota/client')
  const client = new ClientBuilder()
    .node('https://api.lb-0.h.chrysalis-devnet.iota.cafe')
    .build()
  client.getInfo().then(console.log).catch(console.error)
  let DIDDocument;
  // get indexation data by index
  let message_ids;
  await client.getMessage().index(index).then(messageIds => {
    message_ids = messageIds;
    console.log(messageIds);
  }).catch(error => console.log(error));
  let newestMessage = null;
  let timestampnewest = null;
  for (message_id of message_ids) {
    //search newest message: 
    console.log("message: " + message_id);
    await client.getMessage().metadata(message_id).then(async metadata => {
      let milestoneIndex = metadata.referencedByMilestoneIndex;
      console.log("milestoneIndex= " + milestoneIndex);
      await client.getMilestone(milestoneIndex).then(milestone => {
        let timestamp = milestone.timestamp;
        console.log("timestamp: " + timestamp);
        if (newestMessage == null) {
          console.log("timestamp is null");
          timestampnewest = timestamp;
          newestMessage = message_id;
        } else if (timestamp > timestampnewest) {
          console.log(timestamp + " is newer then " + timestampnewest);
          timestampnewest = timestamp;
          newestMessage = message_id;
        }
      });
    });
  }
  console.log("newest message is: " + newestMessage + " with timestamp: " + timestampnewest);
  if (newestMessage && timestampnewest) {
    const message_wrapper = await client.getMessage().data(newestMessage);
    console.log(Buffer.from(message_wrapper.message.payload.data, 'hex').toString('utf8'));
    DIDDocument = Buffer.from(message_wrapper.message.payload.data, 'hex').toString('utf8');
    res.json(JSON.parse(DIDDocument));
  } else {
    // index not found on tangle
    console.log("index not found on tangle");
    res.status(404).send("index not found on tangle");
  }

})

module.exports = router;
